cpl (7.3.2+ds-1) unstable; urgency=medium

  * New upstream version 7.3.2+ds
  * Rediff patches
  * Add new symbols for libcpldfs and libcplcore

 -- Ole Streicher <olebole@debian.org>  Sat, 01 Jul 2023 13:18:41 +0200

cpl (7.2.3+ds-1) unstable; urgency=medium

  * New upstream version 7.2.3+ds
  * Rediff patches
  * Push Standards-Version to 4.6.2. No changes

 -- Ole Streicher <olebole@debian.org>  Thu, 22 Dec 2022 14:30:50 +0100

cpl (7.2.2+ds-1) unstable; urgency=medium

  * New upstream version 7.2.2+ds
  * Rediff patches
  * Push Standards-Version to 4.6.0. No changes needed.
  * Add new symbols to symbol table

 -- Ole Streicher <olebole@debian.org>  Wed, 11 May 2022 21:08:33 +0200

cpl (7.1.4+ds-1) unstable; urgency=medium

  * Remove prebuilt html files from sources
  * New upstream version 7.1.4+ds
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Mon, 16 Aug 2021 20:31:20 +0200

cpl (7.1.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Fix day-of-week for changelog entry 6.3-1~exp1.

  [ Ole Streicher ]
  * Push Standards-Version to 4.5.1. No changes needed
  * Remove +dfsg suffix: no removal anymore
  * New upstream version 7.1.3
  * Rediff patches
  * Push dh-compat to 13. Add not-installed files to d/missing
  * Add "Rules-Requires-Root: no" to d/control

 -- Ole Streicher <olebole@debian.org>  Wed, 10 Feb 2021 15:40:41 +0100

cpl (7.1.2+dfsg-1) unstable; urgency=low

  * Exclude prebuilt HTML from source
  * New upstream version 7.1.2+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required.
  * Push compat to 12
  * Remove old compatibility (build) dependencies for cfitsio
  * Update symbols list for libcplcore26
  * Add gitlab-ci.yml for salsa
  * Explicitly call doxygen for -doc generation

 -- Ole Streicher <olebole@debian.org>  Wed, 31 Jul 2019 08:40:38 +0200

cpl (7.1-3) unstable; urgency=low

  * Further increase of tolerance, for armel, armhf, and mips

 -- Ole Streicher <olebole@debian.org>  Mon, 12 Mar 2018 20:12:59 +0100

cpl (7.1-2) unstable; urgency=low

  * Loose cpl_polynomial-test to fix FTBFS on arm64, 390x, ppc64el

 -- Ole Streicher <olebole@debian.org>  Mon, 12 Mar 2018 14:14:26 +0100

cpl (7.1-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * New upstream version 7.1. Rediff patches. Update symbols table
  * Don't install static libs: not built anymore
  * Push compat to 11
  * Use https for ESO URLs
  * Remove Multi-Arch from libcpl-dev, since it depends on libcext-dev

 -- Ole Streicher <olebole@debian.org>  Mon, 12 Mar 2018 12:50:26 +0100

cpl (7.0-6) unstable; urgency=medium

  * Workround unligned access on sparc64 by not running an optimised loop
    there. (Closes: #846094)
  * Switch to gbp patch workflow

 -- Ole Streicher <olebole@debian.org>  Fri, 08 Sep 2017 17:26:05 +0200

cpl (7.0-5) unstable; urgency=medium

  * Remove Multi-Arch: same for libcext-dev (Closes: #874244)
  * Push standards-version to 4.1.0. No changes needed.
  * Drop libcplgasgano26 package

 -- Ole Streicher <olebole@debian.org>  Wed, 06 Sep 2017 17:50:44 +0200

cpl (7.0-4) unstable; urgency=low

  [ Iain Lane ]
  * Workround unligned access on armhf by not running an optimised loop
    there. Closes: #846094

  [ Ole Streicher ]
  * Add Multi-Arch entries for libcext-dev and libcext-doc
  * Push Standards-Version to 4.0.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Mon, 10 Jul 2017 14:34:41 +0200

cpl (7.0-3) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix "Too many commas in name 4" bibtex issue

  [ Ole Streicher ]
  * Fix angle tolerance in i386
  * Mark libcpl-dev as Multi-Arch: same as recommended by MultiArch hinter
  * Update Standards-Version to 3.9.8. No changes

 -- Ole Streicher <olebole@debian.org>  Fri, 21 Oct 2016 17:07:05 +0200

cpl (7.0-2) unstable; urgency=medium

  * Switch off optimization of cpl_imagelist_basic.c on MIPS. Closes: #813777

 -- Ole Streicher <olebole@debian.org>  Sat, 20 Feb 2016 14:13:30 +0100

cpl (7.0-1) unstable; urgency=low

  * New upstream version. Switch back to unstable.
  * Update d/u/metadata and VCS info

 -- Ole Streicher <olebole@debian.org>  Fri, 19 Feb 2016 09:28:42 +0100

cpl (7.0~b1-1) experimental; urgency=low

  * New upstream beta version. Switch to experimental.
  * Temporarily re-enable all tests.
  * Prepare transition for new SONAME versions
  * Update standards version to 3.9.7. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Tue, 02 Feb 2016 16:14:51 +0100

cpl (6.6.1-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Wed, 22 Jul 2015 14:16:41 +0200

cpl (6.6-1) unstable; urgency=low

  * Add basic upstream/metadata (no reference yet)
  * switch back to unstable

 -- Ole Streicher <olebole@debian.org>  Sat, 25 Apr 2015 12:43:36 +0200

cpl (6.6-1~exp) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Tue, 10 Mar 2015 09:53:36 +0100

cpl (6.6~b-1) experimental; urgency=low

  * New upstream beta version
  * Remove timestamp macros from source for reproducibility

 -- Ole Streicher <olebole@debian.org>  Tue, 24 Feb 2015 09:28:53 +0100

cpl (6.5.1-1~exp1) experimental; urgency=low

  * New upstream version
  * Upload to experimental due to jessie freeze

 -- Ole Streicher <olebole@debian.org>  Thu, 20 Nov 2014 17:33:34 +0100

cpl (6.5-1) unstable; urgency=low

  * New upstream version
  * Build-depend on libcfitsio-dev instead of libcfitsio3-dev. Closes: #761722
  * Update uploader e-mail
  * Update standards version to 3.9.6. No changes needed.
  * Change VCS to debian-astro

 -- Ole Streicher <olebole@debian.org>  Sat, 27 Sep 2014 22:39:28 +0200

cpl (6.4.2-5) unstable; urgency=medium

  * Fix memory leak when cpl_matrix fails. Closes: #753089

 -- Ole Streicher <debian@liska.ath.cx>  Sun, 06 Jul 2014 15:22:55 +0200

cpl (6.4.2-4) unstable; urgency=medium

  * Fix test failure  on mips64el.

 -- Ole Streicher <debian@liska.ath.cx>  Thu, 03 Jul 2014 18:28:14 +0200

cpl (6.4.2-3) unstable; urgency=low

  * Workround FTBS on armhf: disable cpl_ffftw-test

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 16 Jun 2014 09:36:51 +0200

cpl (6.4.2-2) unstable; urgency=low

  * Include symbols files

 -- Ole Streicher <debian@liska.ath.cx>  Sun, 15 Jun 2014 21:58:14 +0200

cpl (6.4.2-1) unstable; urgency=low

  * New upstream version.

 -- Ole Streicher <debian@liska.ath.cx>  Tue, 10 Jun 2014 09:03:48 +0200

cpl (6.4.1-5) unstable; urgency=low

  * Add jquery.js to missing-sources

 -- Ole Streicher <debian@liska.ath.cx>  Sun, 30 Mar 2014 13:26:42 +0200

cpl (6.4.1-4) unstable; urgency=low

  * Disable another test (cpl_aperture) on armel.

 -- Ole Streicher <debian@liska.ath.cx>  Sat, 29 Mar 2014 17:22:21 +0100

cpl (6.4.1-3) unstable; urgency=low

  * Disable cpl_image_basic and cpl_fit tests to workaround FTBS on
    armel and s390x
  * Change maintainer to debian-astro

 -- Ole Streicher <debian@liska.ath.cx>  Sat, 29 Mar 2014 14:12:31 +0100

cpl (6.4.1-2) unstable; urgency=low

  * Force serial tests

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 19 Mar 2014 13:58:37 +0100

cpl (6.4.1-1) unstable; urgency=low

  * New upstream version.
  * Disable parallel build to see build failures

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 19 Mar 2014 09:51:14 +0100

cpl (6.4-1) unstable; urgency=low

  * New upstream version
  * Set standards version to 3.9.5. No changes needed.

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 17 Mar 2014 13:39:38 +0100

cpl (6.3.1-1) unstable; urgency=low

  * Upload to unstable.

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 28 Aug 2013 10:11:58 +0200

cpl (6.3.1-1~exp2) experimental; urgency=low

  * Fix FTBS on big-endian machines. Thanks to Julian Taylor for the patch.

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 26 Aug 2013 09:25:12 +0200

cpl (6.3.1-1~exp1) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 17 May 2013 15:20:52 +0200

cpl (6.3-1~exp1) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Tue, 19 Mar 2013 14:55:21 +0100

cpl (6.2-1~exp2) experimental; urgency=low

  * Correct required version of cfitsio. Closes: #696488

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 26 Dec 2012 17:22:19 +0100

cpl (6.2-1~exp1) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 26 Nov 2012 11:00:59 +0100

cpl (6.1.1-2) unstable; urgency=low

  * Remove compile-time estimation of L2 cache size
  * Disable run cpl_mask and cpl_image_iqe tests. Closes: #677967, #677968

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 20 Jun 2012 09:00:00 +0200

cpl (6.1.1-1) unstable; urgency=low

  * New upstream version
  * Re-insert package libcext-dev. Closes: #671675, #676759, #676762
  * Set DM-Upload-Allowed
  * Loose required accuracy for some unit tests

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 11 Jun 2012 10:00:00 +0200

cpl (6.0-1) unstable; urgency=low

  * New upstream version
  * Add multiarch support. Thanks to Julian Taylor for help.

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 13 Apr 2012 10:30:00 +0200

cpl (5.3.1-2) unstable; urgency=low

  * Fix compilation error on kfreebsd-*, hurd-i386

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 07 Dec 2011 10:44:00 +0100

cpl (5.3.1-1) unstable; urgency=low

  * New package. Closes: #641628

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 11 Nov 2011 17:38:00 +0200
